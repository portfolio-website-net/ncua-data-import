SELECT CONVERT(date, [fs220].[CYCLE_DATE]) AS [Date],
	   CONVERT(decimal, [fs220].[ACCT_018]) AS [Total Amount of Shares and Deposits],
	   CONVERT(decimal, [fs220].[ACCT_083]) AS [Number of current members],
	   CONVERT(decimal, [fs220A].[Acct_564A]) AS [Number of Full-Time credit union employees],	   
	   CONVERT(decimal, [fs220A].[Acct_564B]) AS [Number of Part-Time credit union employees],
	   CONVERT(decimal, [fs220].[Acct_010]) AS [Total Assets],
	   CONVERT(decimal, [fs220A].[Acct_997]) AS [Total Net Worth]
  FROM [fs220]
  LEFT JOIN [fs220A] ON [fs220].[CU_NUMBER] = [fs220A].[CU_NUMBER] AND [fs220].[CYCLE_DATE] = [fs220A].[CYCLE_DATE]
  LEFT JOIN [fs220B] ON [fs220].[CU_NUMBER] = [fs220B].[CU_NUMBER] AND [fs220].[CYCLE_DATE] = [fs220B].[CYCLE_DATE]
  LEFT JOIN [fs220C] ON [fs220].[CU_NUMBER] = [fs220C].[CU_NUMBER] AND [fs220].[CYCLE_DATE] = [fs220C].[CYCLE_DATE]
  LEFT JOIN [fs220D] ON [fs220].[CU_NUMBER] = [fs220D].[CU_NUMBER] AND [fs220].[CYCLE_DATE] = [fs220D].[CYCLE_DATE]
  LEFT JOIN [fs220E] ON [fs220].[CU_NUMBER] = [fs220E].[CU_NUMBER] AND [fs220].[CYCLE_DATE] = [fs220E].[CYCLE_DATE]
  LEFT JOIN [fs220G] ON [fs220].[CU_NUMBER] = [fs220G].[CU_NUMBER] AND [fs220].[CYCLE_DATE] = [fs220G].[CYCLE_DATE]
  LEFT JOIN [fs220H] ON [fs220].[CU_NUMBER] = [fs220H].[CU_NUMBER] AND [fs220].[CYCLE_DATE] = [fs220H].[CYCLE_DATE]
  LEFT JOIN [fs220I] ON [fs220].[CU_NUMBER] = [fs220I].[CU_NUMBER] AND [fs220].[CYCLE_DATE] = [fs220I].[CYCLE_DATE]
  LEFT JOIN [fs220J] ON [fs220].[CU_NUMBER] = [fs220J].[CU_NUMBER] AND [fs220].[CYCLE_DATE] = [fs220J].[CYCLE_DATE]
  LEFT JOIN [fs220K] ON [fs220].[CU_NUMBER] = [fs220K].[CU_NUMBER] AND [fs220].[CYCLE_DATE] = [fs220K].[CYCLE_DATE]
  LEFT JOIN [fs220L] ON [fs220].[CU_NUMBER] = [fs220L].[CU_NUMBER] AND [fs220].[CYCLE_DATE] = [fs220L].[CYCLE_DATE]
  LEFT JOIN [fs220M] ON [fs220].[CU_NUMBER] = [fs220M].[CU_NUMBER] AND [fs220].[CYCLE_DATE] = [fs220M].[CYCLE_DATE]
  LEFT JOIN [fs220N] ON [fs220].[CU_NUMBER] = [fs220N].[CU_NUMBER] AND [fs220].[CYCLE_DATE] = [fs220N].[CYCLE_DATE]
 WHERE [fs220].[CU_NUMBER] = (SELECT TOP 1 [CU_NUMBER]
                                FROM [Credit Union Branch Information]
							   WHERE [CU_NAME] = 'NAVY FEDERAL CREDIT UNION'
							     AND [MainOffice] = 'Yes')
 ORDER BY CONVERT(date, [fs220].[CYCLE_DATE]) DESC