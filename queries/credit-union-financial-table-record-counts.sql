SELECT [Tables].name AS [Table Name],
       SUM([Partitions].[rows]) AS [Total Row Count]
  FROM sys.tables AS [Tables]
  JOIN sys.partitions AS [Partitions] ON [Tables].[object_id] = [Partitions].[object_id]
   AND [Partitions].index_id IN (0, 1)
 WHERE [Tables].name IN (
	    'fs220',
		'fs220A',
		'fs220B',
		'fs220C',
		'fs220D',
		'fs220E',
		'fs220G',
		'fs220H',
		'fs220I',
		'fs220J',
		'fs220K',
		'fs220L',
		'fs220M',
		'fs220N')
 GROUP BY [Tables].name