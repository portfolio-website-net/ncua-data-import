SELECT cubi.[CU_NUMBER],
       cubi.[CU_NAME] AS [Name],
	   cubi.[PhysicalAddressCity] AS [City],
	   cubi.[PhysicalAddressStateCode] AS [State],
	   CONVERT(decimal, [fs220].[ACCT_018]) AS [Total Amount of Shares and Deposits]
  FROM [Credit Union Branch Information] cubi
 INNER JOIN [fs220] ON cubi.[CU_NUMBER] = [fs220].[CU_NUMBER] AND cubi.[CYCLE_DATE] = [fs220].[CYCLE_DATE]
 WHERE [MainOffice] = 'Yes'
   AND cubi.[CYCLE_DATE] = (SELECT TOP 1 [CYCLE_DATE]
                              FROM [fs220]
						     ORDER BY CONVERT(date, [CYCLE_DATE]) DESC)
 ORDER BY CONVERT(decimal, [fs220].[Acct_010]) DESC