SELECT 'Financial Table Fields' AS [Title],
       (SELECT COUNT(1) AS [Field Count]
		  FROM
		  (
			SELECT DISTINCT 
				   ad.[TableName] AS [Table Name],
				   ad.[Account] AS [Field Name]       
			  FROM INFORMATION_SCHEMA.COLUMNS c
			 INNER JOIN [AcctDesc] ad ON c.[COLUMN_NAME] = ad.[Account] 
					AND c.[TABLE_NAME] = ad.[TableName]
			 WHERE c.[TABLE_NAME] IN (
				   'fs220',
				   'fs220A',
				   'fs220B',
				   'fs220C',
				   'fs220D',
				   'fs220E',
				   'fs220G',
				   'fs220H',
				   'fs220I',
				   'fs220J',
				   'fs220K',
				   'fs220L',
				   'fs220M',
				   'fs220N')
		  ) t
		) AS [Total]
 UNION
 SELECT 'Total Unique Credit Unions' AS [Title],
       (SELECT COUNT(1) AS [Field Count]
		  FROM
		  (
			SELECT DISTINCT
				   cubi.[CU_NUMBER],
				   cubi.[CU_NAME] AS [Name]
			  FROM [Credit Union Branch Information] cubi
			 WHERE [MainOffice] = 'Yes'
		  ) t
		) AS [Total]
 UNION
 SELECT 'Most Recent Quarter Credit Unions' AS [Title],
       (SELECT COUNT(1) AS [Field Count]
		  FROM
		  (
			SELECT cubi.[CU_NUMBER],
				   cubi.[CU_NAME] AS [Name]
			  FROM [Credit Union Branch Information] cubi
			 INNER JOIN [fs220] ON cubi.[CU_NUMBER] = [fs220].[CU_NUMBER] AND cubi.[CYCLE_DATE] = [fs220].[CYCLE_DATE]
			 WHERE [MainOffice] = 'Yes'
			   AND cubi.[CYCLE_DATE] = (SELECT TOP 1 [CYCLE_DATE]
										  FROM [fs220]
										 ORDER BY CONVERT(date, [CYCLE_DATE]) DESC)
		  ) t
		) AS [Total]
 UNION
 SELECT 'Sum Total Amount of Shares and Deposits (Most Recent Quarter Credit Unions)' AS [Title],
       (SELECT [Total Amount of Shares and Deposits]
		  FROM
		  (
			SELECT SUM(CONVERT(decimal, [fs220].[ACCT_018])) AS [Total Amount of Shares and Deposits]
			  FROM [Credit Union Branch Information] cubi
			 INNER JOIN [fs220] ON cubi.[CU_NUMBER] = [fs220].[CU_NUMBER] AND cubi.[CYCLE_DATE] = [fs220].[CYCLE_DATE]
			 WHERE [MainOffice] = 'Yes'
			   AND cubi.[CYCLE_DATE] = (SELECT TOP 1 [CYCLE_DATE]
										  FROM [fs220]
										 ORDER BY CONVERT(date, [CYCLE_DATE]) DESC)
		  ) t
		) AS [Total]
 UNION
 SELECT 'Average Total Amount of Shares and Deposits (Most Recent Quarter Credit Unions)' AS [Title],
       (SELECT [Avg Total Amount of Shares and Deposits]
		  FROM
		  (
			SELECT AVG(CONVERT(decimal, [fs220].[ACCT_018])) AS [Avg Total Amount of Shares and Deposits]
			  FROM [Credit Union Branch Information] cubi
			 INNER JOIN [fs220] ON cubi.[CU_NUMBER] = [fs220].[CU_NUMBER] AND cubi.[CYCLE_DATE] = [fs220].[CYCLE_DATE]
			 WHERE [MainOffice] = 'Yes'
			   AND cubi.[CYCLE_DATE] = (SELECT TOP 1 [CYCLE_DATE]
										  FROM [fs220]
										 ORDER BY CONVERT(date, [CYCLE_DATE]) DESC)
		  ) t
		) AS [Total]
 UNION
 SELECT 'Total Financial Table Records' AS [Title],
       (SELECT SUM([Total Row Count])
		  FROM
		  (
			SELECT [Tables].name AS [Table Name],
				   SUM([Partitions].[rows]) AS [Total Row Count]
			  FROM sys.tables AS [Tables]
			  JOIN sys.partitions AS [Partitions] ON [Tables].[object_id] = [Partitions].[object_id]
			   AND [Partitions].index_id IN (0, 1)
			 WHERE [Tables].name IN (
					'fs220',
					'fs220A',
					'fs220B',
					'fs220C',
					'fs220D',
					'fs220E',
					'fs220G',
					'fs220H',
					'fs220I',
					'fs220J',
					'fs220K',
					'fs220L',
					'fs220M',
					'fs220N')
			 GROUP BY [Tables].name
		  ) t
		) AS [Total]