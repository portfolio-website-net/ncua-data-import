SELECT DISTINCT 
       ad.[TableName] AS [Table Name],
       ad.[Account] AS [Field Name], 
       (SELECT TOP 1 [AcctName]
	      FROM [AcctDesc] ad2
		 WHERE c.[COLUMN_NAME] = ad2.[Account] 
		   AND c.[TABLE_NAME] = ad2.[TableName]) AS [Account Name],
	   (SELECT TOP 1 [AcctDesc]
	      FROM [AcctDesc] ad2
		 WHERE c.[COLUMN_NAME] = ad2.[Account] 
		   AND c.[TABLE_NAME] = ad2.[TableName]) AS [Account Description]
  FROM INFORMATION_SCHEMA.COLUMNS c
 INNER JOIN [AcctDesc] ad ON c.[COLUMN_NAME] = ad.[Account] 
        AND c.[TABLE_NAME] = ad.[TableName]
 WHERE c.[TABLE_NAME] IN (
       'fs220',
	   'fs220A',
	   'fs220B',
	   'fs220C',
	   'fs220D',
	   'fs220E',
	   'fs220G',
	   'fs220H',
	   'fs220I',
	   'fs220J',
	   'fs220K',
	   'fs220L',
	   'fs220M',
	   'fs220N')
 ORDER BY ad.[TableName],
          ad.[Account]