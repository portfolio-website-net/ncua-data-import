﻿namespace NcuaDataImport
{
    public class Settings
    {
        public string DbConnectionString { get; set; }

        public string BaseDataUrl { get; set; }

        public string DataHtmlPath { get; set; }

        public int MaxValueLength { get; set; }
    }
}
