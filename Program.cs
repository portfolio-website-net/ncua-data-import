﻿using NcuaDataImport.Services;
using NcuaDataImport.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace NcuaDataImport
{
    public class Program
    {        
        public static void Main(string[] args)
        {
            var services = new ServiceCollection();
            ConfigureServices(services);
            using (var serviceProvider = services.BuildServiceProvider())
            {
                var dataImportService = serviceProvider.GetService<IDataImportService>();
                dataImportService.ImportFilesLinkedFromUrl();
            }
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IDataImportService, DataImportService>();
            services.AddTransient<IDatabaseService, DatabaseService>();

            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            var configuration = builder.Build();

            services.AddOptions();
            services.Configure<Settings>(configuration.GetSection("Settings"));
        }
    }
}
