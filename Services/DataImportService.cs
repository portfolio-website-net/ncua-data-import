﻿using System;
using CsvHelper;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Net;
using FastMember;
using System.IO.Compression;
using HtmlAgilityPack;
using System.Threading.Tasks;
using System.Diagnostics;
using Microsoft.Extensions.Options;
using NcuaDataImport.Services.Interfaces;

namespace NcuaDataImport.Services
{
    public class DataImportService : IDataImportService
    {
        private Dictionary<string, List<string>> _tableColumns = new Dictionary<string, List<string>>();
        private readonly object _statusLockObj = new object();    
        private readonly object _urlLockObj = new object();
        private readonly object _tableSchemaLockObj = new object();
        private Stopwatch _stopwatch = new Stopwatch();

        private readonly IDatabaseService _databaseService;
        private readonly Settings _settings;

        public DataImportService(IDatabaseService databaseService, IOptions<Settings> settings)
        {
            _databaseService = databaseService;
            _settings = settings.Value;
        }

        public void ImportFilesLinkedFromUrl()
        {
            _stopwatch.Start();

            _databaseService.ClearImportedData();

            using (var client = new WebClient())
            {
                var htmlCode = client.DownloadString(_settings.BaseDataUrl + _settings.DataHtmlPath);
                var doc = new HtmlDocument();
                doc.LoadHtml(htmlCode);
                var links = new List<string>();
                foreach (var link in doc.DocumentNode.SelectNodes("//a[@href]"))
                {
                    var attr = link.Attributes["href"];
                    foreach (var linkValue in attr.Value.Split(' '))
                    {
                        if (linkValue.EndsWith(".zip") && !links.Contains(linkValue))
                        {
                            links.Add(_settings.BaseDataUrl + linkValue);
                        }
                    }
                }

                var linkCount = 0;
                Parallel.ForEach(
                    links,
                    new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount },
                    link =>
                    {                        
                        ProcessZipFileUrl(link);

                        lock (_statusLockObj)
                        {
                            linkCount++;
                            var percentComplete = ((float)linkCount/(float)links.Count()).ToString("0.00%");
                            var timeElapsed = _stopwatch.Elapsed.ToString("hh\\:mm\\:ss");
                            Console.WriteLine($"Status: {linkCount} of {links.Count()} ({percentComplete}) - {timeElapsed}");
                        }
                    }
                );
            }

            _stopwatch.Stop();
            Console.WriteLine("Total time elapsed: {0:hh\\:mm\\:ss}", _stopwatch.Elapsed);
        }

        private void ProcessZipFileUrl(string url)
        {
            using (var zip = new ZipArchive(GetStreamFromUrl(url), ZipArchiveMode.Read))
            {
                foreach (var entry in zip.Entries.Where(x => x.Name.EndsWith(".txt")))
                {
                    var tableName = entry.Name.Replace(".txt", string.Empty);
                    using (var stream = entry.Open())
                    {
                        ImportData(url, tableName, stream);
                    }
                }
            }
        }

        private Stream GetStreamFromUrl(string url)
        {
            byte[] imageData = null;

            lock (_urlLockObj)
            {                
                using (var wc = new WebClient())
                {
                    imageData = wc.DownloadData(url);
                }
            }
                
            return new MemoryStream(imageData);            
        }

        private void ImportData(string url, string tableName, Stream inputStream)
        {
            using (var reader = new StreamReader(inputStream))
            using (var csv = new CsvReader(reader))
            {
                csv.Configuration.HasHeaderRecord = false;
                csv.Configuration.BadDataFound = null;
                var records = csv.GetRecords<dynamic>().ToList();

                try
                {
                    var table = CreateDataTable(tableName, records);

                    using (var lockObject = new LockService(tableName))
                    {
                        var lockedKey = lockObject.GetLock();
                        
                        if (IsTableSchemaUpdated(table))
                        {
                            _databaseService.RunSql(_databaseService.GetCreateTableSql(table));
                            _databaseService.RunSql(_databaseService.GetAddColumnsThatDoNotExistSql(table));
                        }

                        _databaseService.ImportData(table);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"URL: {url}");
                    Console.WriteLine($"Table Name: {tableName}");
                    Console.WriteLine($"Error: {ex.Message}");
                    throw;
                }                
            }
        }

        private bool IsTableSchemaUpdated(DataTable table)
        {
            lock (_tableSchemaLockObj)
            {
                var isUpdated = false;
                var newTableColumns = new List<string>();
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    newTableColumns.Add(table.Columns[i].ColumnName);
                }

                if (!_tableColumns.ContainsKey(table.TableName))
                {                
                    _tableColumns.Add(table.TableName, newTableColumns);
                    isUpdated = true;
                }
                else
                {
                    if (newTableColumns.Except(_tableColumns[table.TableName]).Any())
                    {
                        isUpdated = true;
                    }
                }

                return isUpdated;
            }
        }

        private DataTable CreateDataTable(string tableName, IEnumerable<dynamic> records)
        {
            var table = new DataTable(tableName);
            if (records.Any())
            {                
                var firstRecordWithColumnNames = ((IDictionary<string, object>)records.First());
                var columns = firstRecordWithColumnNames.Keys.ToArray();

                var columnNameDict = new Dictionary<string, string>();
                var newColumnList = new List<string>();
                foreach (var column in columns)
                {
                    var newColumnName = GetTrimmedColumnName(firstRecordWithColumnNames[column].ToString());
                    if (IsValidColumnName(newColumnName))
                    {
                        var dataColumn = new DataColumn();
                        dataColumn.DataType = System.Type.GetType("System.String");
                        
                        var i = 2;
                        while (columnNameDict.ContainsValue(newColumnName))
                        {
                            newColumnName = GetTrimmedColumnName(firstRecordWithColumnNames[column].ToString()) + "_" + i++;
                        }
                                            
                        newColumnList.Add(column);
                        columnNameDict.Add(column, newColumnName);
                        dataColumn.ColumnName = newColumnName;
                        table.Columns.Add(dataColumn);
                    }
                }

                foreach (var record in records.Skip(1))
                {
                    var data = (IDictionary<string, object>)record;

                    var row = table.NewRow();
                    foreach (var column in newColumnList)
                    {
                        if (data.ContainsKey(column))
                        {
                            row[columnNameDict[column]] = GetTrimmedValue(data[column].ToString());
                        }
                    }

                    table.Rows.Add(row);
                }

                table.AcceptChanges();
            }

            return table;
        }

        private bool IsValidColumnName(string columnName)
        {
            return !string.IsNullOrEmpty(columnName.Trim())
                && !float.TryParse(columnName.Trim(), out float valueParsed);
        }

        private string GetTrimmedColumnName(string columnName)
        {
            var maxColumnLength = 128 - 3;
            if (columnName.Length < maxColumnLength) 
            {
                return columnName;
            }
            else
            {
                return columnName.Substring(0, maxColumnLength);
            }
        }

        private string GetTrimmedValue(string value)
        {
            if (!string.IsNullOrEmpty(value)
                && !string.IsNullOrEmpty(value.Trim()))
            {
                if (value.Length < _settings.MaxValueLength) 
                {
                    return value;
                }
                else
                {
                    return value.Substring(0, _settings.MaxValueLength);
                }
            }
            else
            {
                return null;
            }
        }
    }
}
