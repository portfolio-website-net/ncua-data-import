﻿namespace NcuaDataImport.Services.Interfaces
{
    public interface IDataImportService
    {
        void ImportFilesLinkedFromUrl();        
    }
}
