﻿using System;

namespace NcuaDataImport.Services.Interfaces
{
    public interface ILockService : IDisposable
    {
        string GetLock();
    }
}
