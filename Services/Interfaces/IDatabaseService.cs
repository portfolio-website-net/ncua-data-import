﻿using System.Data;

namespace NcuaDataImport.Services.Interfaces
{
    public interface IDatabaseService
    {        
        void ClearImportedData();        

        string GetTableDropStatement(string tableName);
        
        void ImportData(DataTable table);         

        void RunSql(string sql);        

        string GetCreateTableSql(DataTable table);
        
        string GetAddColumnsThatDoNotExistSql(DataTable table);        
    }
}
