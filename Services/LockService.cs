﻿using System;
using System.Collections.Generic;
using System.Threading;
using NcuaDataImport.Services.Interfaces;

namespace NcuaDataImport.Services
{
    // Source: https://stackoverflow.com/questions/157511/using-lock-on-the-key-of-a-dictionarystring-object/36621353#36621353
    public class LockService : ILockService
    {
        private static readonly Dictionary<string, string> Lockedkeys = new Dictionary<string, string>();
        private static readonly object CriticalLock = new object();

        private readonly string _key;
        private bool _isLocked;

        public LockService(string key)
        {
            _key = key;

            lock (CriticalLock)
            {
                // If the dictionary doesn't contain the key, add it
                if (!Lockedkeys.ContainsKey(key))
                {
                    Lockedkeys.Add(key, key.ToCharArray().ToString()); // Ensure that the two objects have different references
                }
            }
        }

        public string GetLock()
        {
            var key = Lockedkeys[_key];

            if (!_isLocked)
            {
                Monitor.Enter(key);
            }
            _isLocked = true;

            return key;
        }

        public void Dispose()
        {
            var key = Lockedkeys[_key];

            if (_isLocked)
            {
                Monitor.Exit(key);
            }
            _isLocked = false;
        }
    }
}
