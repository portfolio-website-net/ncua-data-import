﻿using System;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Extensions.Options;
using NcuaDataImport.Services.Interfaces;

namespace NcuaDataImport.Services
{
    public class DatabaseService : IDatabaseService
    {
        private readonly Settings _settings;

        public DatabaseService(IOptions<Settings> settings)
        {
            _settings = settings.Value;
        }

        public void ClearImportedData()
        {
            var sql = new StringBuilder();
            sql.Append(GetTableDropStatement("Acct_Des"));
            sql.Append(GetTableDropStatement("Acct_Desc"));
            sql.Append(GetTableDropStatement("Acct_Restructure_Info"));
            sql.Append(GetTableDropStatement("AcctDesc"));
            sql.Append(GetTableDropStatement("Acct-DescCUSO"));            
            sql.Append(GetTableDropStatement("Acct-DescGrants"));
            sql.Append(GetTableDropStatement("Acct-DescTradeNames"));            
            sql.Append(GetTableDropStatement("ATM"));
            sql.Append(GetTableDropStatement("ATM Locations"));            
            sql.Append(GetTableDropStatement("Credit Union Branch Information"));
            sql.Append(GetTableDropStatement("foicu"));
            sql.Append(GetTableDropStatement("FOICUDES"));
            sql.Append(GetTableDropStatement("fs220"));
            sql.Append(GetTableDropStatement("fs220A"));
            sql.Append(GetTableDropStatement("fs220B"));
            sql.Append(GetTableDropStatement("fs220C"));
            sql.Append(GetTableDropStatement("fs220CUSO"));
            sql.Append(GetTableDropStatement("fs220D"));
            sql.Append(GetTableDropStatement("fs220E"));
            sql.Append(GetTableDropStatement("fs220G"));
            sql.Append(GetTableDropStatement("fs220H"));
            sql.Append(GetTableDropStatement("fs220I"));
            sql.Append(GetTableDropStatement("fs220J"));
            sql.Append(GetTableDropStatement("fs220K"));
            sql.Append(GetTableDropStatement("fs220L"));
            sql.Append(GetTableDropStatement("fs220M"));
            sql.Append(GetTableDropStatement("fs220N"));
            sql.Append(GetTableDropStatement("Grants"));
            sql.Append(GetTableDropStatement("Readme"));
            sql.Append(GetTableDropStatement("Report1"));  
            sql.Append(GetTableDropStatement("TradeNames"));
            sql.Append(GetTableDropStatement("AcctDAcct-DescGrantsesc"));
            sql.Append(GetTableDropStatement("BuildVersion"));    

            using (var connection = new SqlConnection(_settings.DbConnectionString))
            {
                var command = new SqlCommand(sql.ToString(), connection);
                connection.Open();
                command.ExecuteNonQuery();
            }
        }

        public string GetTableDropStatement(string tableName)
        {
            return $"IF OBJECT_ID('dbo.[{tableName}]', 'U') IS NOT NULL BEGIN DROP TABLE dbo.[{tableName}] END\n";
        }

        
        public void ImportData(DataTable table)
        {
            using (var bulkCopy = new SqlBulkCopy(_settings.DbConnectionString))
            {
                bulkCopy.DestinationTableName = $"dbo.[{table.TableName}]";

                for (int i = 0; i < table.Columns.Count; i++)
                {
                    bulkCopy.ColumnMappings.Add(new SqlBulkCopyColumnMapping(table.Columns[i].ColumnName, table.Columns[i].ColumnName));
                }

                bulkCopy.BulkCopyTimeout = 200;
                bulkCopy.BatchSize = 5000;                

                bulkCopy.WriteToServer(table);
            }
        }        

        public void RunSql(string sql)
        {            
            if (!string.IsNullOrEmpty(sql))
            {
                using (var connection = new SqlConnection(_settings.DbConnectionString))
                {
                    var command = new SqlCommand(sql, connection);
                    connection.Open();
                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(sql);
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                }   
            }         
        }

        public string GetCreateTableSql(DataTable table)
        {
            var sql = new StringBuilder();
            if (table.Columns.Count > 0)
            {
                var addCuNumberTableIndex = false;
                var addCycleDateTableIndex = false;

                sql.Append($"IF OBJECT_ID('dbo.[{table.TableName}]', 'U') IS NULL\n");
                sql.Append($"BEGIN\n");
                sql.Append($"CREATE TABLE dbo.[{table.TableName}] (");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    if (table.Columns[i].ColumnName.ToUpper() == "CU_NUMBER")
                    {
                        addCuNumberTableIndex = true;
                    }
                    else if (table.Columns[i].ColumnName.ToUpper() == "CYCLE_DATE")
                    {
                        addCycleDateTableIndex = true;
                    }

                    sql.Append($"[{table.Columns[i].ColumnName}]");
                    sql.Append($" nvarchar({_settings.MaxValueLength})");
                    sql.Append($",");
                }

                sql.Remove(sql.Length - 1, 1);
                sql.Append($");\n");

                if (addCuNumberTableIndex)
                {
                    sql.Append($"CREATE NONCLUSTERED INDEX [IX_{table.TableName}_CU_NUMBER] ON dbo.[{table.TableName}] (CU_NUMBER)\n");                    
                }

                if (addCycleDateTableIndex)
                {                    
                    sql.Append($"CREATE NONCLUSTERED INDEX [IX_{table.TableName}_CYCLE_DATE] ON dbo.[{table.TableName}] (CYCLE_DATE)\n");
                }
                
                sql.Append($"END");
            }

            return sql.ToString();
        }

        public string GetAddColumnsThatDoNotExistSql(DataTable table)
        {
            var sql = new StringBuilder();            
            for (int i = 0; i < table.Columns.Count; i++)
            {
                sql.Append($"IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[{table.TableName}]') AND name = '{table.Columns[i].ColumnName}')\n");
                sql.Append($"BEGIN ALTER TABLE [dbo].[{table.TableName}] ADD [{table.Columns[i].ColumnName}] nvarchar(max) END\n");
            }

            return sql.ToString();
        }
    }
}
